import json

json_file = open('scripts/core/sample.json', 'r')

d = dict(json.load(json_file))


def print_nested(data):
    # if isinstance(data,str):
    #   print("string")
    if not isinstance(data, str):
        for k, v in data.items():
            if isinstance(v, dict):
                print_nested(v)
            elif hasattr(v, '__iter__') and not isinstance(v, str):
                for item in v:
                    print_nested(item)
            elif isinstance(v, str):
                if k == "color":
                    uni_color.append(v)
            else:
                if k == "color":
                    uni_color.append(v)
    return set(uni_color)


uni_color = []
uni_color_set = print_nested(d)
print(len(uni_color_set))
print(list(uni_color_set))
